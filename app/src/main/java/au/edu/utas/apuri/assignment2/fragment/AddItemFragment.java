package au.edu.utas.apuri.assignment2.fragment;


import android.Manifest;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.categories.Category;
import au.edu.utas.apuri.assignment2.utility.AppUtility;
import au.edu.utas.apuri.assignment2.viewmodel.AddItemViewModel;
import au.edu.utas.apuri.assignment2.viewmodel.ListViewModel;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddItemFragment extends BaseFragment {

    private static final int SELECT_PICTURE = 1;

    private String selectedImagePath;
    Button btnShoppingList;
    ListViewModel listViewModel;
    private Button btnAddCategory;
    List<String> categories = new ArrayList<>();

    private EditText etItemName;

    AddItemViewModel addItemViewModel;
    private ArrayAdapter<String> spinnerArrayAdapter;

    Button btnPickCategory, btnDone;
    private Spinner spinner;
    private TextView tvSelectedCategory;
    private EditText etQuantity;
    private EditText etBuyAt;
    private Spinner spinnerUnit;
    private String listId;
    private TextView tvCaptureImage, tvPickImage;
    private ImageView ivProduct;
    private Uri file;
    private Button purchasedList;
    ShoppingItem shoppingItem;
    Boolean isEdit = false;
    Boolean isGallery = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (getArguments().getString("listId") != null) {
            listId = getArguments().getString("listId");
        }
        View view = inflater.inflate(R.layout.fragment_add_item, container, false);
        listViewModel = ViewModelProviders.of(getActivity()).get(ListViewModel.class);
        addItemViewModel = ViewModelProviders.of(getActivity()).get(AddItemViewModel.class);

        initView(view);
        return view;
    }

    public static AddItemFragment getInstance(String listId) {
        Bundle bundle = new Bundle();
        bundle.putString("listId", listId);
        AddItemFragment addItemFragment = new AddItemFragment();
        addItemFragment.setArguments(bundle);
        return addItemFragment;
    }

    private void initView(View view) {
        btnShoppingList = view.findViewById(R.id.btnShoppingList);
        btnShoppingList.setOnClickListener(this);

        btnAddCategory = view.findViewById(R.id.btnAddCategory);
        btnAddCategory.setOnClickListener(this);

        btnPickCategory = view.findViewById(R.id.btnPickCategory);
        btnPickCategory.setOnClickListener(this);

        btnDone = view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(this);

        purchasedList = view.findViewById(R.id.btnPurchasedList);
        purchasedList.setOnClickListener(this);

        tvCaptureImage = view.findViewById(R.id.tvCaptureImage);
        tvCaptureImage.setOnClickListener(this);

        tvPickImage = view.findViewById(R.id.tvPickImage);
        tvPickImage.setOnClickListener(this);

        ivProduct = view.findViewById(R.id.ivProduct);

        etItemName = view.findViewById(R.id.etItemName);
        etQuantity = view.findViewById(R.id.etQuantity);
        etBuyAt = view.findViewById(R.id.etBuyAt);
        spinnerUnit = view.findViewById(R.id.spinnerUnit);
        spinner = view.findViewById(R.id.categorySpinner);

        tvSelectedCategory = view.findViewById(R.id.tvCategorySelected);
        setCategorySpinner();
        if (getArguments().getParcelable("shoppingItem") != null) {
            isEdit = true;
            shoppingItem = getArguments().getParcelable("shoppingItem");
            listId = shoppingItem.getListId();
            etItemName.setText(shoppingItem.getName());
            etQuantity.setText(String.valueOf(shoppingItem.getQuantity()));
            spinnerUnit.setSelection(Arrays.asList(getResources().getStringArray(R.array.qtyArr)).indexOf(shoppingItem.getUnit()));
            tvSelectedCategory.setText(shoppingItem.getCategory());
            etBuyAt.setText(shoppingItem.getBuyAt());
            spinner.setSelection(categories.indexOf(shoppingItem.getCategory()));

        } else {
            isEdit = false;
            shoppingItem = new ShoppingItem();
        }


        createListObserver();
        createCategoryObserver();

        listViewModel.getList();

    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takePicture();
            }
        }
    }

    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

        startActivityForResult(intent, 100);
    }

    private void showAddCategoryDialog() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_add_category);
        // Set dialog title
        dialog.setTitle(getString(R.string.add_category));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // set values for custom dialog components - text, image and button
        final EditText text = (EditText) dialog.findViewById(R.id.tvItemName);
        Button btnAdd = (Button) dialog.findViewById(R.id.btnAdd);

        dialog.show();

        // if decline button is clicked, close the custom dialog
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                if (text.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.add_category_empty_alert), Toast.LENGTH_SHORT).show();
                    return;
                }
                addItemViewModel.insertCategory(new Category(UUID.randomUUID().toString(), text.getText().toString().trim()));
                Toast.makeText(getActivity(), getString(R.string.category_added), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK && file != null) {
                isGallery = false;
                ivProduct.setImageURI(file);
            }
        }
        if (requestCode == SELECT_PICTURE) {
            isGallery = true;
            Uri selectedImageUri = data.getData();
            selectedImagePath = getPath(getActivity().getApplicationContext(), selectedImageUri);
            file = selectedImageUri;
            ivProduct.setImageURI(selectedImageUri);
        }
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    private void setCategorySpinner() {
        // spinner = new Spinner(getActivity(), Spinner.MODE_DIALOG);

        spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                categories);
        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvSelectedCategory.setText(categories.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnAddCategory:
                showAddCategoryDialog();
                break;
            case R.id.btnPickCategory:
                if (categories.size() > 0) {
                    spinner.performClick();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.no_category_added), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnDone:
                addItemToDb();
                break;
            case R.id.tvCaptureImage:
                if (checkPermissions()) {
                    takePicture();
                }
                break;
            case R.id.tvPickImage:
                if (checkPermissions()) {
                    pickPicture();
                }
                break;
            case R.id.btnPurchasedList:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, PurchasedItemsFragment.getInstance(listId))
                        .addToBackStack(AddItemFragment.class.getName()).commit();
                break;
            case R.id.btnShoppingList:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer,
                        ListCategoryFragment.getInstance(listId))
                        .addToBackStack(this.getClass().getName()).commit();
                break;
            default:
                break;
        }

    }

    private void pickPicture() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);
    }

    private void addItemToDb() {
        String name = etItemName.getText().toString().trim();
        String quantity = etQuantity.getText().toString().trim();
        String unit = getResources().getStringArray(R.array.qtyArr)[spinnerUnit.getSelectedItemPosition()];
        if (name == null || name.isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.name_empty_error), Toast.LENGTH_SHORT).show();

        } else if (quantity == null || quantity.isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.quantity_empty_error), Toast.LENGTH_SHORT).show();
        } else {
            if (listId == null) {
                Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                String filePath = "";
                if (file != null) {
                    if (isGallery)
                        filePath = file.toString();
                    else
                        filePath = file.getPath();
                }
                if (isEdit) {

                   /* shoppingItem = new ShoppingItem(name, listId, Integer.parseInt(quantity), 0, unit, spinner.getSelectedItem().toString(),
                            etBuyAt.getText().toString().trim(), filePath, AppUtility.convertTimeToDate(
                            Calendar.getInstance().getTimeInMillis()));*/

                    shoppingItem.setName(name);
                    shoppingItem.setListId(listId);
                    shoppingItem.setQuantity(Integer.parseInt(quantity));
                    shoppingItem.setUnit(unit);
                    shoppingItem.setCategory(tvSelectedCategory.getText().toString().trim());
                    shoppingItem.setBuyAt(etBuyAt.getText().toString());
                    shoppingItem.setProductPath(filePath);
                    shoppingItem.setTimestamp(AppUtility.convertTimeToDate(Calendar.getInstance().getTimeInMillis()));

                } else {
                    shoppingItem = new ShoppingItem(name, listId, Integer.parseInt(quantity), 0, unit, tvSelectedCategory.getText().toString().trim(),
                            etBuyAt.getText().toString().trim(), filePath, AppUtility.convertTimeToDate(
                            Calendar.getInstance().getTimeInMillis()));
                }

                if (listViewModel.insert(shoppingItem) > 0) {
                    Toast.makeText(getActivity(), getString(R.string.item_added), Toast.LENGTH_SHORT).show();
                    resetFields();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        }
        btnShoppingList.setText("Shopping List (" + addItemViewModel.getItemsByListId(listId).size() + ")");

    }

    private void resetFields() {
        etItemName.getText().clear();
        etQuantity.getText().clear();
        etBuyAt.getText().clear();
        etItemName.requestFocus();
        ivProduct.setImageResource(0);
    }

    private void createListObserver() {
        btnShoppingList.setText("Shopping List (" + addItemViewModel.getItemsByListId(listId).size() + ")");

       /* addItemViewModel.getItemsByListId(listId).observe(this, new Observer<List<ShoppingList>>() {
            @Override
            public void onChanged(@Nullable final java.util.List<ShoppingList> words) {
                // Update the cached copy of the words in the adapter.
                btnShoppingList.setText("Shopping List ("+words.size()+")");

            }
        });*/
    }

    private void createCategoryObserver() {
        addItemViewModel.getCategories().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable final java.util.List<String> words) {
                // Update the cached copy of the words in the adapter.
                categories.clear();
                categories.addAll(words);
                spinnerArrayAdapter.notifyDataSetChanged();

            }
        });
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }
}
