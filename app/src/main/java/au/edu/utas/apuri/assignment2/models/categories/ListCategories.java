package au.edu.utas.apuri.assignment2.models.categories;

import java.util.ArrayList;

import au.edu.utas.apuri.assignment2.models.ShoppingItem;

/**
 * Created by Jagnesh on 05-May-18.
 */

public class ListCategories {
    private String categoryName;
    private ArrayList<ShoppingItem> categoryItems;

    public ListCategories(){

    }
    public ListCategories(String categoryName, ArrayList<ShoppingItem> categoryItems) {
        this.categoryName = categoryName;
        this.categoryItems = categoryItems;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<ShoppingItem> getCategoryItems() {
        return categoryItems;
    }

    public void setCategoryItems(ArrayList<ShoppingItem> categoryItems) {
        this.categoryItems = categoryItems;
    }
}
