package au.edu.utas.apuri.assignment2.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.categories.ListCategories;



public class CategoryItemListAdapter extends RecyclerView.Adapter<CategoryItemListAdapter.ViewHolder> {

    Context context;
    ArrayList<ListCategories> categoryList;
    CategoryItemsAdapter.CategoryEventListener categoryEventListener;

    public CategoryItemListAdapter(Context context, ArrayList<ListCategories> categoryList, CategoryItemsAdapter.CategoryEventListener categoryEventListener) {
        this.context = context;
        this.categoryList = categoryList;
        this.categoryEventListener=categoryEventListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ListCategories listCategory = categoryList.get(position);
        holder.tvCategoryName.setText(listCategory.getCategoryName());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        holder.rvCategoryItems.setLayoutManager(layoutManager);

        CategoryItemsAdapter categoryItemsAdapter = new CategoryItemsAdapter(context, listCategory.getCategoryItems(),categoryEventListener
               );
        holder.rvCategoryItems.setAdapter(categoryItemsAdapter);

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        RecyclerView rvCategoryItems;
        TextView tvCategoryName;
        public ViewHolder(View itemView) {
            super(itemView);
            rvCategoryItems = itemView.findViewById(R.id.rvCategoryItems);
            tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
        }
    }
}
