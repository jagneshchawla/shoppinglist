package au.edu.utas.apuri.assignment2.listeners;

import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;

public interface ShoppingListListener {
    void onItemClick(ShoppingList shoppingList);
    void onListTitleClick(ShoppingList shoppingList);
    void onListDeleteClick(ShoppingList shoppingList);
    void onItemDeleteClick(ShoppingItem shoppingItem);
    void onListEditClick(ShoppingList shoppingList);
    void onItemEditClick(ShoppingItem shoppingItem);
}
