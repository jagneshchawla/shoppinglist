package au.edu.utas.apuri.assignment2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.listeners.ShoppingItemListener;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;


public class ShoppingListItemAdapter extends RecyclerView.Adapter<ShoppingListItemAdapter.ViewHolder> {
    Context context;
    ArrayList<ShoppingItem> listItems=new ArrayList<>();
    ShoppingItemListener shoppingItemListener;
    public ShoppingListItemAdapter(Context context, ArrayList<ShoppingItem> listItems, ShoppingItemListener shoppingItemListener) {
        this.context = context;
        this.shoppingItemListener = shoppingItemListener;
        if(listItems!=null) {
            this.listItems = listItems;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_list_item_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvListItemName.setText(listItems.get(position).getName());
        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoppingItemListener.onItemDeleteClick(listItems.get(position));
            }
        });
        holder.editItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoppingItemListener.onItemEditClick(listItems.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvListItemName;
        public ImageButton deleteItem,editItem;

        ViewHolder(View itemView) {
            super(itemView);
            tvListItemName = (TextView) itemView.findViewById(R.id.tvListItemName);
            deleteItem = (ImageButton) itemView.findViewById(R.id.deleteItem);
            editItem = itemView.findViewById(R.id.editItem);

        }

        @Override
        public void onClick(View view) {

        }
    }
}
