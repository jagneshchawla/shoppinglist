package au.edu.utas.apuri.assignment2.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import au.edu.utas.apuri.assignment2.constant.AppConstants;


public class BaseActivity extends AppCompatActivity implements AppConstants,View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onClick(View v) {

    }


}
