package au.edu.utas.apuri.assignment2.fragment;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.activity.BaseActivity;
import au.edu.utas.apuri.assignment2.adapters.ShoppingListAdapter;
import au.edu.utas.apuri.assignment2.listeners.ShoppingListListener;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.utility.AppUtility;
import au.edu.utas.apuri.assignment2.utility.FragmentUtility;
import au.edu.utas.apuri.assignment2.viewmodel.ListViewModel;


public class ShoppingListFragment extends BaseFragment {

    Button btnCreateList;
    FragmentUtility fragmentUtility;
    RecyclerView rvShoppingList;
    ShoppingListAdapter shoppingListAdapter;

    private ListViewModel listViewModel;
    private List<ShoppingList> list=new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listViewModel = ViewModelProviders.of(this).get(ListViewModel.class);
        //addDummyData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shoping_list, container, false);
        initView(view);

        return view;
    }
    private void initView(View view){
        getActivity().onAttachFragment(this);

        rvShoppingList = (RecyclerView) view.findViewById(R.id.rvShoppingList);
        rvShoppingList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL,false));

        btnCreateList = (Button) view.findViewById(R.id.btnCreateList);
        btnCreateList.setOnClickListener(this);
        fragmentUtility = new FragmentUtility((BaseActivity)getActivity());
        shoppingListAdapter = new ShoppingListAdapter(getActivity(), (ArrayList<ShoppingList>) list, new ShoppingListListener() {
            @Override
            public void onItemClick(ShoppingList shoppingList) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer,
                        new ListCategoryFragment())
                .addToBackStack(this.getClass().getName()).commit();
            }

            @Override
            public void onListTitleClick(ShoppingList shoppingList) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer,
                        AddItemFragment.getInstance(shoppingList.getId()))
                        .addToBackStack(this.getClass().getName()).commit();

            }

            @Override
            public void onListDeleteClick(final ShoppingList shoppingList) {
                AppUtility.showAlertWithActions(getActivity(), "Alert", "Are you sure to delete?", "Yes", "No", new AppUtility.ActionButtons() {
                    @Override
                    public void okAction() {
                        listViewModel.deleteList(shoppingList);
                        list.remove(shoppingList);
                        shoppingListAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void cancelAction() {

                    }
                });

            }

            @Override
            public void onItemDeleteClick(ShoppingItem shoppingItem) {
                listViewModel.deleteItem(shoppingItem);
            }

            @Override
            public void onListEditClick(ShoppingList shoppingList) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("shoplist",shoppingList);
                AddListFragment addListFragment= new AddListFragment();
                addListFragment.setArguments(bundle);
                fragmentUtility.loadFragment(addListFragment,R.id.llContainer,"AddListFragment",true);
            }

            @Override
            public void onItemEditClick(ShoppingItem shoppingItem) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("shoppingItem",shoppingItem);
                AddItemFragment addItemFragment = new AddItemFragment();
                addItemFragment.setArguments(bundle);
                fragmentUtility.loadFragment(addItemFragment,R.id.llContainer,"AddItemFragment",true);
            }

        });
        rvShoppingList.setAdapter(shoppingListAdapter);
        if(getArguments()!=null && getArguments().getParcelableArrayList("list")!=null){
            list.clear();
            ((ArrayList<ShoppingList>) list).addAll(getArguments().<ShoppingList>getParcelableArrayList("list"));
            shoppingListAdapter.notifyDataSetChanged();

        }
        else {
            createListObserver();
        }
    }


    private void createListObserver() {
        listViewModel.getList().observe(this, new Observer<java.util.List<ShoppingList>>() {
            @Override
            public void onChanged(@Nullable final java.util.List<ShoppingList> words) {
                // Update the cached copy of the words in the adapter.
                list.clear();
                list.addAll(words);
                shoppingListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnCreateList){
            fragmentUtility.loadFragment(new AddListFragment(),R.id.llContainer,"AddListFragment",true);
        }
    }

    private ArrayList<ShoppingList> addDummyData(){
        ArrayList<ShoppingList> lists = new ArrayList<>();

      /*  listViewModel.insert(new ShoppingItem("item 1", UUID.randomUUID().toString(),1,100,"pcs"));
        listViewModel.insert(new ShoppingItem("item 2",UUID.randomUUID().toString(),1,100,"pcs"));
        listViewModel.insert(new ShoppingItem("item 3",UUID.randomUUID().toString(),1,100,"pcs"));
        listViewModel.insert(new ShoppingItem("item 4",UUID.randomUUID().toString(),1,100,"pcs"));
        listViewModel.insert(new ShoppingItem("item 5",UUID.randomUUID().toString(),1,100,"pcs"));

*/
        return lists;

      }
}
