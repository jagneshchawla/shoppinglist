package au.edu.utas.apuri.assignment2.Database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.ArrayList;
import java.util.List;

import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ShoppingItemDao {
    @Query("select * from ShoppingItem")
    LiveData<List<ShoppingItem>> getShoppingList();

    @Query("select DISTINCT category from ShoppingItem")
    List<String> getShoppingItemsByCat();

    @Query("select * from ShoppingItem where id = :id")
    ShoppingItem getItembyId(String id);


    @Insert(onConflict = REPLACE)
    long addItem(ShoppingItem item);

    @Delete
    int deleteItem(ShoppingItem item);



    @Query("select * from ShoppingItem where listId = :id")
    List<ShoppingItem> getItemsByListId(String id);

    @Query("select * from ShoppingItem where category = :cat and listId= :listId")
    List<ShoppingItem> getItemsByCat(String cat,String listId);

    @Query("UPDATE ShoppingItem SET purchased = :purchased  WHERE id = :id")
    long setPurchased(int purchased,int id);

    @Query("select * from ShoppingItem where purchased = 1 and listId= :listId")
    LiveData<List<ShoppingItem>> getPurchasedItems(String listId);

    @Query("select * from ShoppingItem where name=:name OR timestamp = :selectedTimestamp ")
    List<ShoppingItem> searchItems(String selectedTimestamp, String name);
}
