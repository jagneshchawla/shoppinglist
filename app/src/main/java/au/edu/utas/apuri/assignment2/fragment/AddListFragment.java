package au.edu.utas.apuri.assignment2.fragment;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.utility.AppUtility;
import au.edu.utas.apuri.assignment2.viewmodel.ListViewModel;

public class AddListFragment extends BaseFragment {
    EditText etListName;

    Button btnAdd;
    ListViewModel listViewModel;
    Bundle bundle= null;
    ShoppingList shoppingList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_list, container, false);
        initView(view);
        return view;
    }
    private void initView(View view){
        etListName = (EditText) view.findViewById(R.id.etListName);
        btnAdd=view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        /*etListName.requestFocus();
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);*/
        shoppingList = new ShoppingList();
        listViewModel = ViewModelProviders.of(this).get(ListViewModel.class);
        bundle = getArguments();
        if (bundle != null){
        shoppingList = bundle.getParcelable("shoplist");
        etListName.setText(shoppingList.getListName().toString());
        }else {
            shoppingList = new ShoppingList(UUID.randomUUID().toString(),etListName.getText().toString().trim()
                    ,new ArrayList<ShoppingItem>(), AppUtility.convertTimeToDate(Calendar.getInstance().getTimeInMillis()));
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btnAdd:
                if(etListName.getText().toString().isEmpty()){

                    return;
                }
                shoppingList.setListName(etListName.getText().toString().trim());
                AppUtility.hideSoftKeyboard(getActivity());
                listViewModel.insertList(shoppingList);
                etListName.getText().clear();
                listAddedDialog();
                break;
        }
    }

    private void listAddedDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
                builder.setTitle(getString(R.string.app_name))
                        .setMessage("List Created Successfully.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                })
               /* .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
