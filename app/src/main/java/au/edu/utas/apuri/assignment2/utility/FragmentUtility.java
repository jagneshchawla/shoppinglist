package au.edu.utas.apuri.assignment2.utility;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import au.edu.utas.apuri.assignment2.activity.BaseActivity;


public class FragmentUtility {
    Context context;
    FragmentManager fragmentManager;
    public FragmentUtility(BaseActivity context){
        this.context=context;
        fragmentManager=context.getSupportFragmentManager();
    }

    /**
     * Method to load fragment
     * @param fragment to be loaded
     * @param container to which fragment to be loaded
     * @param tag for fragment
     */
    public void loadFragment(Fragment fragment, int container, String tag, boolean addToBackStack){
       FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
                if(addToBackStack)
                    fragmentTransaction.addToBackStack(tag);
                fragmentTransaction.replace(container,fragment)
                .commit();
    }


}
