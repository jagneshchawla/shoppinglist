package au.edu.utas.apuri.assignment2.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.utility.AppUtility;


public class PurchasedItemsAdapter extends RecyclerView.Adapter<PurchasedItemsAdapter.ViewHolder> {

    Context context;
    ArrayList<ShoppingItem> items;
    PurchasedEventListener purchasedEventListener;
    private int lastFocussedPosition = -1;
    private Handler handler = new Handler();
    public PurchasedItemsAdapter(Context context, ArrayList<ShoppingItem> items,PurchasedEventListener purchasedEventListener) {
        this.context = context;
        this.items = items;
        this.purchasedEventListener=purchasedEventListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchased_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ShoppingItem shoppingItem=items.get(position);
        holder.tvPurchaseItem.setText(shoppingItem.getName());
        holder.tvQuantity.setText(shoppingItem.getQuantity()+" "+shoppingItem.getUnit());
        holder.etPrice.setText(shoppingItem.getPrice()+"");
        holder.etPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    handler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            if (lastFocussedPosition == -1 || lastFocussedPosition == position) {
                                lastFocussedPosition = position;
                                holder.etPrice.requestFocus();
                            }
                        }
                    }, 200);

                } else {
                    lastFocussedPosition = -1;
                }
            }
        });
        holder.updatePrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    purchasedEventListener.onPriceChange(Integer.parseInt(holder.etPrice.getText().toString()),position);
                    AppUtility.showAlertWithActions(context, "Alert", "Price updated", "OK", null, new AppUtility.ActionButtons() {
                        @Override
                        public void okAction() {

                        }

                        @Override
                        public void cancelAction() {

                        }
                    });
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        });
       /* holder.etPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    purchasedEventListener.onPriceChange(Integer.parseInt(editable.toString()),position);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvPurchaseItem;
        TextView tvQuantity;
        EditText etPrice;
        ImageButton updatePrice;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPurchaseItem = itemView.findViewById(R.id.tvPurchaseItem);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            etPrice=itemView.findViewById(R.id.etPrice);
            updatePrice=itemView.findViewById(R.id.updatePrice);
        }
    }

    public interface PurchasedEventListener{
        void onPriceChange(int price,int item);
    }
}
