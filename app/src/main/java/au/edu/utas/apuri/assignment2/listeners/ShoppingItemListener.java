package au.edu.utas.apuri.assignment2.listeners;

import au.edu.utas.apuri.assignment2.models.ShoppingItem;

/**
 * Created by Jagnesh on 23-May-18.
 */

public interface ShoppingItemListener {
    void onItemDeleteClick(ShoppingItem shoppingItem);
    void onItemEditClick(ShoppingItem shoppingItem);
}
