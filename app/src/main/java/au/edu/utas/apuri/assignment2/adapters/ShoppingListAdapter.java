package au.edu.utas.apuri.assignment2.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.listeners.ShoppingItemListener;
import au.edu.utas.apuri.assignment2.listeners.ShoppingListListener;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.utility.AppUtility;

public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ViewHolder> {

    Context context;
    ArrayList<ShoppingList> list;
    ShoppingListListener shoppingListListener;

    public ShoppingListAdapter(Context context, ArrayList<ShoppingList> list,ShoppingListListener shoppingListListener) {
        this.context = context;
        this.list = list;
        this.shoppingListListener=shoppingListListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_list_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ShoppingList list = this.list.get(position);
        holder.tvListName.setText(list.getListName());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        holder.rvListItems.setLayoutManager(linearLayoutManager);
        //holder.rvListItems.setLayoutManager(new LinearLayoutManager(context, LinearLayout.VERTICAL,false));
        holder.shoppingListItemAdapter = new ShoppingListItemAdapter(context, list.getItems(), new ShoppingItemListener() {
            @Override
            public void onItemDeleteClick(final ShoppingItem shoppingItem) {
                AppUtility.showAlertWithActions(context, "Alert", "Are you sure to delete?", "Yes", "No", new AppUtility.ActionButtons() {
                    @Override
                    public void okAction() {
                        shoppingListListener.onItemDeleteClick(shoppingItem);
                        list.getItems().remove(shoppingItem);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void cancelAction() {

                    }
                });

            }

            @Override
            public void onItemEditClick(ShoppingItem shoppingItem) {
                shoppingListListener.onItemEditClick(shoppingItem);
            }
        });
        holder.rvListItems.setAdapter(holder.shoppingListItemAdapter);
        holder.tvListName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shoppingListListener!=null){
                    shoppingListListener.onListTitleClick(list);
                }
            }
        });
        holder.llRv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoppingListListener.onItemClick(list);
            }
        });
        holder.deleteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoppingListListener.onListDeleteClick(list);
            }
        });
        holder.editList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoppingListListener.onListEditClick(list);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvListName;
        RecyclerView rvListItems;
        LinearLayout llRv;
        ImageButton editList,deleteList;
        ShoppingListItemAdapter shoppingListItemAdapter;

        ViewHolder(View itemView) {
            super(itemView);
            tvListName= (TextView) itemView.findViewById(R.id.tvListName);
            rvListItems= (RecyclerView) itemView.findViewById(R.id.rvListItems);
            llRv= (LinearLayout) itemView.findViewById(R.id.llRv);
            editList= (ImageButton) itemView.findViewById(R.id.editList);
            deleteList= (ImageButton) itemView.findViewById(R.id.deleteList);


        }
    }
}
