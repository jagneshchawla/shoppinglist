package au.edu.utas.apuri.assignment2.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import au.edu.utas.apuri.assignment2.Database.AppDatabase;
import au.edu.utas.apuri.assignment2.Database.CategoryDao;
import au.edu.utas.apuri.assignment2.Database.ShoppingItemDao;
import au.edu.utas.apuri.assignment2.Database.ShoppingListDao;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.models.categories.Category;

public class ShoppingRepository  {
    private LiveData<List<ShoppingItem>> shoppingList=new MutableLiveData<>();
    private MutableLiveData<List<ShoppingList>> lists=new MutableLiveData<>();
    private ShoppingItemDao shoppingDao;

    LiveData<List<String>> categories;
    private ShoppingListDao shoppingListDao;
    private CategoryDao categoryDao;

    public ShoppingRepository(Application application) {
        AppDatabase db = AppDatabase.getInstance(application);
        shoppingDao = db.shoppingDao();
        shoppingListDao = db.shoppingListDao();
        categoryDao = db.categoryDao();
    }

    public LiveData<List<ShoppingItem>> getShoppingItems() {
        shoppingList=shoppingDao.getShoppingList();
        return shoppingList;
    }
    public List<String> getShoppingItemsByCategories() {
        return shoppingDao.getShoppingItemsByCat();
    }

    public List<ShoppingItem> getItemsByListId(String id) {
        return shoppingDao.getItemsByListId(id);
    }


    public List<ShoppingItem> getItemsByCat(String cat,String listId){
        return shoppingDao.getItemsByCat(cat,listId);
    }

    public LiveData<List<ShoppingList>> getShoppingList() {
        List<ShoppingList> list = shoppingListDao.getShoppingList();
        for(ShoppingList shoppingList:list){
            shoppingList.setItems((ArrayList<ShoppingItem>) shoppingDao.getItemsByListId(shoppingList.getId()));
            Log.d("listSize",shoppingList.getItems().size()+"");
        }
        lists.setValue(list);

        return lists;
    }

    public LiveData<List<String>> getCategories() {
        categories = categoryDao.getCategoryList();
        return categories;
    }

    public void insertCategory(Category category) {
         categoryDao.addCategory(category);
    }


    public void insertList(ShoppingList shoppingList) {
        shoppingListDao.addItem(shoppingList);
    }


    public long insert (ShoppingItem item) {
       return shoppingDao.addItem(item);
    }

    public int deleteItem(ShoppingItem word) {
        return shoppingDao.deleteItem(word);
    }
    public void deleteList(ShoppingList list) {
         shoppingListDao.deleteItem(list);
    }



    public void setPurchased(ShoppingItem item) {
       shoppingDao.setPurchased(item.getPurchased(),item.getId());
    }

    public LiveData<List<ShoppingItem>> getPurchasedItems(String listId) {
        return shoppingDao.getPurchasedItems(listId);
    }

    public List<ShoppingList> searchItems(String selectedTimestamp, String name) {
        List<ShoppingList> list = shoppingListDao.searchShoppinglist(name,selectedTimestamp);
        for(ShoppingList shoppingList:list){
            shoppingList.setItems((ArrayList<ShoppingItem>) shoppingDao.getItemsByListId(shoppingList.getId()));
            Log.d("listSize",shoppingList.getItems().size()+"");
        }

        return list;
    }
}
