package au.edu.utas.apuri.assignment2.fragment;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.adapters.PurchasedItemsAdapter;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.viewmodel.ListViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class PurchasedItemsFragment extends Fragment {

    RecyclerView rvPurchasedItems;
    PurchasedItemsAdapter purchasedItemsAdapter;
    private ListViewModel listViewModel;
    private List<ShoppingItem> list=new ArrayList<>();
    private TextView tvTotalExp;
    String listId="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_purchased_items, container, false);
        listViewModel= ViewModelProviders.of(getActivity()).get(ListViewModel.class);
        initView(view);
        createListObserver();

        return view;
    }
    public static PurchasedItemsFragment getInstance(String listId){
        Bundle bundle=new Bundle();
        bundle.putString("listId",listId);
        PurchasedItemsFragment purchasedItemsFragment=new PurchasedItemsFragment();
        purchasedItemsFragment.setArguments(bundle);
        return purchasedItemsFragment;
    }

    private void initView(View view) {
        if(getArguments()!=null){
            listId=getArguments().getString("listId");
        }
        rvPurchasedItems = view.findViewById(R.id.rvPurchasedItems);
        tvTotalExp=view.findViewById(R.id.tvTotalExp);
        rvPurchasedItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayout.VERTICAL,false));
        purchasedItemsAdapter = new PurchasedItemsAdapter(getActivity(), (ArrayList<ShoppingItem>) list, new PurchasedItemsAdapter.PurchasedEventListener() {
            @Override
            public void onPriceChange(int price, int position) {
                list.get(position).setPrice(price);
                listViewModel.insert(list.get(position));
                setTotalExpenditure();
            }
        });
        rvPurchasedItems.setAdapter(purchasedItemsAdapter);


    }

    private void setTotalExpenditure() {
        int total=0;
        for(ShoppingItem item:list){
            total+=item.getPrice();
        }
        tvTotalExp.setText(total+"");
    }

    private void createListObserver() {
        listViewModel.getPurchasedItems(listId).observe(this, new Observer<List<ShoppingItem>>() {
            @Override
            public void onChanged(@Nullable final java.util.List<ShoppingItem> words) {
                // Update the cached copy of the words in the adapter.
                list.clear();
                list.addAll(words);
                purchasedItemsAdapter.notifyDataSetChanged();
                setTotalExpenditure();

            }
        });
    }


}
