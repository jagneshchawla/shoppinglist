package au.edu.utas.apuri.assignment2.utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AppUtility {

    public static String convertTimeToDate(long timestamp){
        Calendar calendar=Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        return sdf.format(calendar.getTime());
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null && inputManager != null) {
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                inputManager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }
    //---- Alert Dialog with Action Callbacks
    public static void showAlertWithActions(Context context, String title, String message, String positiveButton, @Nullable String negativeButton, final ActionButtons actionButtons) {
        AlertDialog.Builder alertDialog;
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                actionButtons.okAction();
            }
        });
        if (negativeButton != null) {
            alertDialog.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    actionButtons.cancelAction();
                }
            });
        }
        alertDialog.show();

    }

    //---- Alert actions
    public interface ActionButtons {
        void okAction();

        void cancelAction();
    }


}
