package au.edu.utas.apuri.assignment2.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.models.categories.Category;
import au.edu.utas.apuri.assignment2.repository.ShoppingRepository;

public class CategoryViewModel extends AndroidViewModel {

    private ShoppingRepository mRepository;

    private LiveData<List<ShoppingItem>> shoppingItemsCat;
    // Create a LiveData with a String

    public CategoryViewModel(Application application) {
        super(application);
        mRepository = new ShoppingRepository(application);
        shoppingItemsCat = mRepository.getShoppingItems();

    }


    public LiveData<List<ShoppingItem>> getShoppingItems() {
        return shoppingItemsCat;
    }

    public List<String> getCategories() {
        return mRepository.getShoppingItemsByCategories();
    }

    public long insert(ShoppingItem word) {
        return mRepository.insert(word);
    }

    public int deleteItem(ShoppingItem word) {
        return mRepository.deleteItem(word);
    }

    public List<ShoppingItem> getItemsByCat(String cat,String listId){
        return mRepository.getItemsByCat(cat,listId);
    }

    public void setPurchased(ShoppingItem item) {
        mRepository.setPurchased(item);
    }
}