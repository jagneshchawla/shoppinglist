package au.edu.utas.apuri.assignment2.fragment;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.adapters.CategoryItemListAdapter;
import au.edu.utas.apuri.assignment2.adapters.CategoryItemsAdapter;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.categories.ListCategories;
import au.edu.utas.apuri.assignment2.utility.AppUtility;
import au.edu.utas.apuri.assignment2.viewmodel.CategoryViewModel;
import au.edu.utas.apuri.assignment2.viewmodel.ListViewModel;


public class ListCategoryFragment extends BaseFragment {
    RecyclerView rvCategoryList;
    CategoryItemListAdapter categoryItemListAdapter;
    String listId="";
    private CategoryViewModel categoryViewModel;
    private ArrayList<ListCategories> alListCategories=new ArrayList<>();
    ArrayList<ShoppingItem> itemsChecked=new ArrayList<>();
    Button btnDelete,btnShare,btnPurchased;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        initView(view);
        return view;
    }
    private void initView(View view){
        if(getArguments()!=null){
            listId=getArguments().getString("listId");
        }
        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);

        btnDelete=view.findViewById(R.id.btnDelete);
        btnPurchased=view.findViewById(R.id.btnPurchased);
        btnShare=view.findViewById(R.id.btnShare);

        btnDelete.setOnClickListener(this);
        btnPurchased.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        rvCategoryList = view.findViewById(R.id.rvCategoryList);
        rvCategoryList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL,false));
        categoryItemListAdapter = new CategoryItemListAdapter(getActivity(),alListCategories,
                new CategoryItemsAdapter.CategoryEventListener() {
                    @Override
                    public void productChecked(ShoppingItem shoppingItem) {
                        if(!itemsChecked.contains(shoppingItem)) {
                            itemsChecked.add(shoppingItem);
                        }
                    }

                    @Override
                    public void productUnChecked(ShoppingItem shoppingItem) {
                        if(itemsChecked.contains(shoppingItem)){
                            itemsChecked.remove(shoppingItem);
                        }
                    }

                    @Override
                    public void productUnPurchase(ShoppingItem shoppingItem) {
                        shoppingItem.setPurchased(0);
                        categoryViewModel.setPurchased(shoppingItem);
                    }
                });
        rvCategoryList.setAdapter(categoryItemListAdapter);
        createCategoryObserver();

    }

    public static ListCategoryFragment getInstance(String listId){
        Bundle bundle=new Bundle();
        bundle.putString("listId",listId);
        ListCategoryFragment addItemFragment=new ListCategoryFragment();
        addItemFragment.setArguments(bundle);
        return addItemFragment;
    }

    private void createCategoryObserver() {
        categoryViewModel.getShoppingItems().observe(this, new Observer<List<ShoppingItem>>() {
            @Override
            public void onChanged(@Nullable final java.util.List<ShoppingItem> words) {
                // Update the cached copy of the words in the adapter.
                alListCategories.clear();
                List<String> categories=categoryViewModel.getCategories();
                for(String cat:categories){
                    List list=categoryViewModel.getItemsByCat(cat,listId);
                    if(list!=null && list.size()>0){
                        alListCategories.add(new ListCategories(cat, (ArrayList<ShoppingItem>) list));
                    }
                }
                categoryItemListAdapter.notifyDataSetChanged();

            }
        });

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btnDelete:
                deleteItems();
            break;
            case R.id.btnPurchased:
                checkItemsPurchased();
                break;
            case R.id.btnShare:
                shareItems();
                break;
            default:
                break;
        }
    }

    private void deleteItems() {
        if(itemsChecked.size()>0){
            for(ShoppingItem item:itemsChecked){
                categoryViewModel.deleteItem(item);
            }
        }
    }
    private void checkItemsPurchased(){
        if(itemsChecked.size()>0){
            for(ShoppingItem item:itemsChecked){
                item.setPurchased(1);
                item.setTimestamp(AppUtility.convertTimeToDate(Calendar.getInstance().getTimeInMillis()));
                categoryViewModel.setPurchased(item);
            }

        }
    }

    private  void shareItems(){
        if(itemsChecked.size()>0){
            StringBuffer stringBuffer = new StringBuffer();
            for(ShoppingItem item:itemsChecked){
                stringBuffer.append("Name: ").append(item.getName()).append("\n");
                stringBuffer.append("Buy At: ").append(item.getBuyAt()).append("\n");
                stringBuffer.append("Qty: ").append(item.getQuantity()).append(item.getUnit()).append("\n");
                stringBuffer.append("---------------------------\n\n");
            }
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Here is the share content body";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Shopping list");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, stringBuffer.toString());
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }

    }
}
