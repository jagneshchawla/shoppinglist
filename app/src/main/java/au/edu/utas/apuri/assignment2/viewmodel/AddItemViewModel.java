package au.edu.utas.apuri.assignment2.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.models.categories.Category;
import au.edu.utas.apuri.assignment2.repository.ShoppingRepository;

public class AddItemViewModel extends AndroidViewModel {

    private ShoppingRepository mRepository;

    private LiveData<List<Category>> categories;
    // Create a LiveData with a String

    public AddItemViewModel (Application application) {
        super(application);
        mRepository = new ShoppingRepository(application);
    }


    public List<ShoppingItem> getItemsByListId(String id)
    {
        return mRepository.getItemsByListId(id);
    }

    public LiveData<List<String>> getCategories()
    {

        return mRepository.getCategories();
    }



    public void insertCategory(Category category) {
        mRepository.insertCategory(category); }

    public void insertList(ShoppingList list) {
        mRepository.insertList(list); }
}
