package au.edu.utas.apuri.assignment2.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.repository.ShoppingRepository;


public class ListViewModel extends AndroidViewModel {

    private ShoppingRepository mRepository;

    private LiveData<List<ShoppingItem>> shoppingItems;
    private LiveData<List<ShoppingList>> list;
    // Create a LiveData with a String

    public ListViewModel(Application application) {
        super(application);
        mRepository = new ShoppingRepository(application);
        shoppingItems = mRepository.getShoppingItems();

    }


    public LiveData<List<ShoppingItem>> getShoppingItems() {
        return shoppingItems;
    }

    public LiveData<List<ShoppingList>> getList() {
        return mRepository.getShoppingList();
    }


    public long insert(ShoppingItem word) {
        return mRepository.insert(word);
    }

    public void deleteList(ShoppingList list) {
        mRepository.deleteList(list);
    }
    public void deleteItem(ShoppingItem item) {
        mRepository.deleteItem(item);
    }

    public void insertList(ShoppingList list) {
        mRepository.insertList(list);
    }

    public LiveData<List<ShoppingItem>> getPurchasedItems(String listId) {
        return mRepository.getPurchasedItems(listId);
    }

    public List<ShoppingList> searchItems(String selectedTimestamp, String name) {
        return mRepository.searchItems(selectedTimestamp, name);
    }
}
