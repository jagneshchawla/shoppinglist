package au.edu.utas.apuri.assignment2.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import au.edu.utas.apuri.assignment2.constant.AppConstants;



public class BaseFragment extends Fragment implements View.OnClickListener,AppConstants {
    @Override
    public void onClick(View v) {

    }
    //--- Hide keyboard --
    public void hideKeyboard(EditText editText){
        InputMethodManager im = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
    public void hideKeyboard(EditText editTexts[]){
        for (EditText editText : editTexts) {
            InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }
}
