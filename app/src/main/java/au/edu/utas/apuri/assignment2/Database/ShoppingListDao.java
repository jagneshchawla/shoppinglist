package au.edu.utas.apuri.assignment2.Database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ShoppingListDao {

    @Query("select * from ShoppingList")
    List<ShoppingList> getShoppingList();

    @Query("select * from ShoppingList WHERE listName=:name OR date=:time")
    List<ShoppingList> searchShoppinglist(String name,String time);

    @Query("select * from ShoppingList where id = :id")
    ShoppingList getItembyId(String id);

    @Insert(onConflict = REPLACE)
    void addItem(ShoppingList item);

    @Delete
    void deleteItem(ShoppingList item);
}
