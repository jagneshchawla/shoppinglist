package au.edu.utas.apuri.assignment2.fragment;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.ShoppingList;
import au.edu.utas.apuri.assignment2.viewmodel.ListViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    Button btnSelectDate;
    private int mYear, mMonth, mDay;
    String selectedTimestamp="";
    ListViewModel listViewModel;
    EditText etName;
    Button btnSearch;
    Spinner searchType;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        initView(view);
        return view;
    }
    private void initView(View view){
        btnSelectDate = view.findViewById(R.id.btnSelectDate);
        listViewModel= ViewModelProviders.of(this).get(ListViewModel.class);
        etName=view.findViewById(R.id.etSearchText);
        btnSearch=view.findViewById(R.id.btnSearch);

        btnSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
// Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedTimestamp=dayOfMonth + "-" + String.format("%02d",monthOfYear + 1) + "-" + year;
                                btnSelectDate.setText(dayOfMonth + "-" + String.format("%02d",monthOfYear + 1) + "-" + year);
                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etName.getText().toString().isEmpty() && selectedTimestamp.isEmpty()){
                    return;
                }
                List<ShoppingList> items=listViewModel.searchItems(selectedTimestamp,etName.getText().toString().trim());
                Log.d("items", items.size() + "");

                if(items!=null && items.size()==0){
                    Toast.makeText(getActivity(),"no matched list found",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    ShoppingListFragment shoppingListFragment = new ShoppingListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("list", (ArrayList<ShoppingList>) items);
                    shoppingListFragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer, shoppingListFragment).
                            addToBackStack(SearchFragment.class.getName()).commit();
                }
            }
        });
    }

}
