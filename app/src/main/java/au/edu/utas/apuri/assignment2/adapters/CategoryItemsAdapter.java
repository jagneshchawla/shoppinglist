package au.edu.utas.apuri.assignment2.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import au.edu.utas.apuri.assignment2.R;
import au.edu.utas.apuri.assignment2.constant.AppConstants;
import au.edu.utas.apuri.assignment2.models.ShoppingItem;

public class CategoryItemsAdapter extends RecyclerView.Adapter<CategoryItemsAdapter.ViewHolder> {

    Context context;
    ArrayList<ShoppingItem> items;
    CategoryEventListener categoryEventListener;

    public CategoryItemsAdapter(Context context, ArrayList<ShoppingItem> items,CategoryEventListener categoryEventListener) {
        this.context = context;
        this.items = items;
        this.categoryEventListener=categoryEventListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_list_shopping_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ShoppingItem item = items.get(position);
        holder.tvItemName.setText(item.getName());
        holder.tvQuantity.setText(item.getQuantity()+" "+item.getUnit());
        try {
            holder.ivProduct.setImageURI(Uri.parse(item.getProductPath()));
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.tvShopName.setText(item.getBuyAt());
        if(item.getPurchased()== 1){
            holder.unPurchase.setVisibility(View.VISIBLE);
            holder.checkBox.setVisibility(View.GONE);
            holder.llMain.setAlpha(0.5f);
            holder.llMain.setBackgroundColor(Color.parseColor("#eeeeee"));
        }
        else{
            holder.unPurchase.setVisibility(View.GONE);
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.llMain.setAlpha(1.0f);
            holder.llMain.setBackgroundColor(Color.WHITE);
        }

        holder.unPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryEventListener.productUnPurchase(item);
            }
        });
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                categoryEventListener.productChecked(item);
                }
                else{
                    categoryEventListener.productUnChecked(item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemName;
        TextView tvQuantity;
        TextView tvShopName;
        ImageView ivProduct;
        CheckBox checkBox;
        LinearLayout llMain;
        ImageButton unPurchase;

        public ViewHolder(View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            ivProduct = itemView.findViewById(R.id.ivProduct);
            checkBox = itemView.findViewById(R.id.checkbox);
            tvShopName = itemView.findViewById(R.id.tvShopName);
            llMain=itemView.findViewById(R.id.llMain);
            unPurchase = itemView.findViewById(R.id.unPurchase);

        }
    }

    public interface CategoryEventListener{
        void productChecked(ShoppingItem shoppingItem);
        void productUnChecked(ShoppingItem shoppingItem);
        void productUnPurchase(ShoppingItem shoppingItem);

    }

}

