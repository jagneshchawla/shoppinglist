package au.edu.utas.apuri.assignment2.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;

/**
 * Created by Jagnesh on 02-May-18.
 */
@Entity
public class ShoppingList implements Parcelable {

    @NonNull
    @PrimaryKey
    private String Id;
    private String listName;
    private String date;

    @Ignore
    private ArrayList<ShoppingItem> items;

    public String getDate() {
        return date;
    }


    public void setDate(String date) {
        this.date = date;
    }

    public ShoppingList(String id, String listName, ArrayList<ShoppingItem> items,String date) {
        this.Id=id;
        this.listName = listName;
        this.items = items;
        this.date=date;
    }

    public ShoppingList(){

    }
    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public ArrayList<ShoppingItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<ShoppingItem> items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Id);
        dest.writeString(this.listName);
        dest.writeString(this.date);
        dest.writeTypedList(this.items);
    }

    protected ShoppingList(Parcel in) {
        this.Id = in.readString();
        this.listName = in.readString();
        this.date = in.readString();
        this.items = in.createTypedArrayList(ShoppingItem.CREATOR);
    }

    public static final Creator<ShoppingList> CREATOR = new Creator<ShoppingList>() {
        @Override
        public ShoppingList createFromParcel(Parcel source) {
            return new ShoppingList(source);
        }

        @Override
        public ShoppingList[] newArray(int size) {
            return new ShoppingList[size];
        }
    };
}
