package au.edu.utas.apuri.assignment2.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

@Entity
public class ShoppingItem implements Parcelable {
    private String name;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    private String listId;
    private int quantity;
    private int price;
    private String unit;
    private String category;
    private String buyAt="";
    private String productPath;
    private String timestamp;
    private int purchased;

    public ShoppingItem(){

    }
    public ShoppingItem(String name, String listId,int quantity, int price, String unit, String category, String buyAt,String filePath
    ,String timestamp) {
        this.name = name;
        this.listId=listId;
        this.quantity = quantity;
        this.price = price;
        this.unit = unit;
        this.category=category;
        this.buyAt=buyAt;
        this.productPath=filePath;
        this.timestamp=timestamp;
    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setPurchased(int purchased) {
        this.purchased = purchased;
    }

    public int getPurchased() {
        return purchased;
    }

    public String getProductPath() {
        return productPath;
    }

    public void setProductPath(String productPath) {
        this.productPath = productPath;
    }

    public String getCategory() {

        return category;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof ShoppingItem) {
            return ((ShoppingItem)other).id==(this.id);
        }
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return this.id;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public String getBuyAt() {
        return buyAt;
    }

    public void setBuyAt(String buyAt) {
        this.buyAt = buyAt;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.id);
        dest.writeString(this.listId);
        dest.writeInt(this.quantity);
        dest.writeInt(this.price);
        dest.writeString(this.unit);
        dest.writeString(this.category);
        dest.writeString(this.buyAt);
        dest.writeString(this.productPath);
        dest.writeString(this.timestamp);
        dest.writeInt(this.purchased);
    }

    protected ShoppingItem(Parcel in) {
        this.name = in.readString();
        this.id = in.readInt();
        this.listId = in.readString();
        this.quantity = in.readInt();
        this.price = in.readInt();
        this.unit = in.readString();
        this.category = in.readString();
        this.buyAt = in.readString();
        this.productPath = in.readString();
        this.timestamp = in.readString();
        this.purchased = in.readInt();
    }

    public static final Parcelable.Creator<ShoppingItem> CREATOR = new Parcelable.Creator<ShoppingItem>() {
        @Override
        public ShoppingItem createFromParcel(Parcel source) {
            return new ShoppingItem(source);
        }

        @Override
        public ShoppingItem[] newArray(int size) {
            return new ShoppingItem[size];
        }
    };
}
