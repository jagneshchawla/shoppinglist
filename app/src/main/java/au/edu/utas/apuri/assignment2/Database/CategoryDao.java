package au.edu.utas.apuri.assignment2.Database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import au.edu.utas.apuri.assignment2.models.ShoppingItem;
import au.edu.utas.apuri.assignment2.models.categories.Category;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface CategoryDao {
    @Query("select name from Category")
    LiveData<List<String>> getCategoryList();

    @Query("select * from Category where id = :id")
    Category getCategory(String id);

    @Insert(onConflict = REPLACE)
    void addCategory(Category item);

    @Delete
    void deleteCategory(Category item);


}
