package au.edu.utas.apuri.assignment2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import au.edu.utas.apuri.assignment2.activity.BaseActivity;
import au.edu.utas.apuri.assignment2.fragment.AddListFragment;
import au.edu.utas.apuri.assignment2.fragment.SearchFragment;
import au.edu.utas.apuri.assignment2.fragment.ShoppingListFragment;
import au.edu.utas.apuri.assignment2.utility.FragmentUtility;

public class MainActivity extends BaseActivity {

    FragmentUtility fragmentUtility;
    ImageView ivBack, ivSearch;
    TextView tvHeader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivSearch = (ImageView) findViewById(R.id.ivSearch);
        tvHeader = (TextView) findViewById(R.id.tvHeader);

        ivBack.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        fragmentUtility = new FragmentUtility(this);
        fragmentUtility.loadFragment(new ShoppingListFragment(), R.id.llContainer, "ShoppingListFragment", false);

    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof ShoppingListFragment) {
            tvHeader.setText("My ShoppingList");
            ivBack.setVisibility(View.GONE);
            ivSearch.setVisibility(View.VISIBLE);
        } else if (fragment instanceof AddListFragment) {
            tvHeader.setText("Add ShoppingList");
            ivBack.setVisibility(View.VISIBLE);
            ivSearch.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.ivBack) {
            onBackPressed();
        } else if (id == R.id.ivSearch) {
            fragmentUtility.loadFragment(new SearchFragment(), R.id.llContainer, "SearchFragment", true);
        }

    }
}
